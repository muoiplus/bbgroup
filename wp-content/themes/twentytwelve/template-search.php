<!--
Template Name: Search Results
 -->

<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
    <div id="primary" class="site-content">
        <gcse:searchresults-only></gcse:searchresults-only>
    </div>
<?php endwhile; // end of the loop. ?>
<?php get_sidebar(); ?>

    <div id="secondary" class="widget-area" role="complementary">
        <aside id="wpp-2" class="widget popular-posts">
            <h3 class="widget-title">Các Tag bài viết</h3>
            <?php
            $tags = get_tags('number=6&order=RAND');
            if ($tags) {
                ?>
                <ul class="wpp-list">
                    <?php foreach ($tags as $tag) { ?>
                        <li>
                            <a href="<?php echo get_tag_link($tag->term_id); ?>"
                               title="<?php echo $tag->name; ?>"><?php echo $tag->name; ?></a>
                        </li>

                    <?php } ?>
                </ul>
            <?php
            }
            wp_reset_postdata(); ?>
        </aside>
    </div>
<?php get_footer(); ?>