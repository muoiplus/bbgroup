<div class="search-wrap">
    <form method="get" action="<?php echo esc_url( home_url( '/' ) ) ?>search-results/">
        <div class="input-wrap">
            <input type="text" id="search-input" name="q" value="<?php the_search_query(); ?>">
            <input type="submit" id="search-submit" value=" ">
        </div>
    </form>
</div>
