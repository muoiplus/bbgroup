<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	</div><!-- #main .wrapper -->

</div><!-- #page -->
<footer id="colophon" role="contentinfo">
    <div class="content-center">
        <p>Trợ giúp   |   Gửi phản hồi   |    Bảo mật và điều khoản   |    <a hrefs="http://becomebettergroup.com/sitemap.xml">Sitemap</a></p>
    </div>
</footer><!-- #colophon -->
<?php wp_footer(); ?>
</body>
</html>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53418381-2', 'auto');
  ga('send', 'pageview');

</script>