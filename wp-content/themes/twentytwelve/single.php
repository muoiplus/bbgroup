<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'content', 'page' ); ?>

				<?php comments_template( '', true ); ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->


<div id="secondary" class="widget-area" role="complementary">
    <aside id="wpp-2" class="widget popular-posts">
        <h3 class="widget-title">Bài viết liên quan</h3>
        <?php
        $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 6, 'post__not_in' => array($post->ID) ) );
        if( $related ) foreach( $related as $post ) {
            setup_postdata($post); ?>
            <ul class="wpp-list">
                <li>
                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                </li>
            </ul>
        <?php }
        wp_reset_postdata(); ?>
    </aside>
</div>
<?php get_sidebar(); ?>
<div id="secondary" class="widget-area" role="complementary">
    <aside id="wpp-2" class="widget popular-posts">
        <h3 class="widget-title">Bài viết mới nhất</h3>
        <?php
        $related = get_posts( array( 'numberposts' => 6, 'post_parent' => $post->ID, 'orderby' => 'post_date' ) );
        $related = query_posts($args);
        if( $related ) foreach( $related as $post ) {
            setup_postdata($post); ?>
            <ul class="wpp-list">
                <li>
                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                </li>
            </ul>
        <?php }
        wp_reset_postdata(); ?>
    </aside>
</div>
<?php get_footer(); ?>